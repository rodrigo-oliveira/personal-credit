# personal-credit-simulator-lib

Micro aplicação para inserir um simulador simples que o usuário preenche o valor, nome, email e após a validação do formulário é recuperado as parcelas disponíveis em um serviço definido na inicialização da lib. 
Após o envio do formulário de simulação é disparado o event emitter 'onSubmit' e através dele é possível recuperar as informações da simulação no componente que utilizar a biblioteca.

## Exemplo de utilização:
app.module.ts
```
import { SimulatorModule } from 'personal-credit-simulator-lib';

@NgModule({
  declarations: [
    AppComponent,
    ExampleComponent
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    AppRoutingModule,
    SimulatorModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
```

example.component.html
```
<lib-simulator
    (onSuccess)="onSuccessGetInstallments($event)"
    (onError)="onErrorGetInstallments($event)"
    (onSubmit)="onSubmitSimulation($event)">
</lib-simulator>
```
example.component.ts
```
export class ExampleComponent implements OnInit {
  installmentsEndpoint = 'http://localhost:3000/parcelas';

  constructor(private router: Router, private simulatorService: SimulatorService) {
  }

  ngOnInit() {
    this.simulatorService.setEndpoint(this.installmentsEndpoint);
  }

  onSuccessGetInstallments(installments) {
    console.log('Sucesso ao recuperar as parcelas:', installments);
  }

  onErrorGetInstallments(error) {
    console.log('Não foi possível recuperar as parcelas do cliente:', error);
  }

  onSubmitSimulation(simulation) {
    console.log('Informações da simulação:', simulation);
    this.router.navigate(['contratacao']);
  }
}
```
## Events
### onSuccess(installments)
* installments - Array - Lista de objetos com as parcelas

Emite o evento ao retornar as parcelas do serviço definido na inicialização do componente.

Retorna array de objetos com os seguintes atributos:
```
{
    installmentValue: 750,
    numberInstallments: 2,
    totalValue: 1500
}
```

### onError(error) 
* error - Array - Lista de objetos com as parcelas

Emite o evento caso o serviço de recuperação das parcelas retorne erro.

### onSubmit(simulation)
* simulation - Object - Lista de objetos com as parcelas

Emite o evento ao submeter o formulário de simulação.

Retorna o seguinte objeto:
```
{
    email: "email@test.com",
    installmentsValue: 2,
    name: "Nome do cliente",
    simulationDate: Sun Dec 08 2019 17:24:52 GMT-0300 (Horário Padrão de Brasília) {},
    totalValue: 1500
}
```

This library was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.2.3.

## Code scaffolding

Run `ng generate component component-name --project simulator` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module --project simulator`.
> Note: Don't forget to add `--project simulator` or else it will be added to the default project in your `angular.json` file. 

## Build

Run `ng build simulator` to build the project. The build artifacts will be stored in the `dist/` directory.

## Publishing

After building your library with `ng build simulator`, go to the dist folder `cd dist/simulator` and run `npm publish`.

## Running unit tests

Run `ng test simulator` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
