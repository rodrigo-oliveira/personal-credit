import {
  Component,
  Input,
  Output,
  EventEmitter,
  OnInit
} from '@angular/core';
import {
  FormGroup,
  FormControl,
  Validators
} from '@angular/forms';
import { Simulation } from '../shared/simulation';
import { SimulatorService } from './simulator.service';
import { Installment } from '../shared/installment';

@Component({
  selector: 'lib-simulator',
  templateUrl: './simulator.component.html',
  styleUrls: ['./simulator.component.scss']
})
export class SimulatorComponent implements OnInit {
  @Output() onSuccess: EventEmitter<any> = new EventEmitter();
  @Output() onError: EventEmitter<any> = new EventEmitter();
  @Output() onSubmit: EventEmitter<any> = new EventEmitter();

  private installments: Installment[] = [];

  simulatorFormGroup: FormGroup;
  installmentsEnabled: Boolean = false;
  installmentsLoading: Boolean = false;
  disabled: Boolean = false;
  invert: Boolean = false;
  thumbLabel: Boolean = true;
  vertical: Boolean = false;
  max: Number  = 24;
  min: Number  = 1;
  step: Number  = 1;
  installmentsValue: Number = 1;
  totalValue: Number = 0;

  constructor(private simulatorService: SimulatorService) {}

  ngOnInit() {
    this.createFormGroup(new Simulation());
  }

  createFormGroup(user: Simulation) {
    this.simulatorFormGroup = new FormGroup(
      {
        name: new FormControl(
          user.name,
          Validators.compose([
            Validators.required,
            Validators.min(300),
            Validators.minLength(2),
            Validators.maxLength(50)
          ])
        ),
        email: new FormControl(
          user.email,
          Validators.compose([
            Validators.required,
            Validators.email
          ])
        ),
        totalValue: new FormControl(
          user.totalValue,
          Validators.compose([
            Validators.required
          ])
        ),
        installmentsValue: new FormControl(
          user.installmentsValue,
          Validators.compose([
            Validators.min(1)
          ])
        ),
        simulationDate: new FormControl(
          new Date()
        )
      }
    )
  }

  formatLabel(value: number) {
    return value + 'x';
  }

  onSubmitSimulation() {
    if (this.simulatorFormGroup.valid) {
      this.onSubmit.emit(this.simulatorFormGroup.value);
    }
  }

  recalcInstallments() {
    this.installmentsEnabled = false;
  }

  onCalculatingInstallments() {
    const totalValue = this.simulatorFormGroup.controls.totalValue.value;

    this.totalValue = totalValue;
    this.installmentsLoading = true;
    this.getInstallments(totalValue);
  }

  private getInstallments(totalValue) {
    this.simulatorService
      .getInstallments(totalValue)
      .subscribe(
        (response) => {
          this.installments = response;
          this.installmentsLoading = false;
          this.installmentsEnabled = true;
          this.min = this.simulatorService.getMinimumInstallment(this.installments);
          this.max = this.simulatorService.getMaximumInstallment(this.installments);
          this.onSuccess.emit(response);
        },
        (error) => {
          this.installmentsLoading = false;
          this.installmentsEnabled = false;
          this.onError.emit(error);
        }
      );
  }

  getInstallment(numberInstallments) {
    return this.installments.filter((item) => item.numberInstallments === numberInstallments)[0];
  }
}
