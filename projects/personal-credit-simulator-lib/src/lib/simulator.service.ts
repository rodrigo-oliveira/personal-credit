import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Installment } from '../shared/installment';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SimulatorService {
  endpoint: string = '';

  constructor(private http: HttpClient) { }

  public setEndpoint(url) {
    this.endpoint = url;
  }

  private getEndpoint() {
    return this.endpoint;
  }

  public getInstallments(totalValue: number): Observable<Installment[]>  {
    const endpoint = this.getEndpoint();
    return this.http.get<Installment[]>(endpoint);
  }

  public getMinimumInstallment(installments) {
    return installments.reduce((min, p) => p.numberInstallments < min ? p.numberInstallments : min, installments[0].numberInstallments);
  }

  public getMaximumInstallment(installments) {
    return installments.reduce((max, p) => p.numberInstallments > max ? p.numberInstallments : max, installments[0].numberInstallments);    
  }
};
