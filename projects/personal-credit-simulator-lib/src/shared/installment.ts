export interface Installment {
    numberInstallments: number;
    installmentValue: number;
    totalValue: number;
};