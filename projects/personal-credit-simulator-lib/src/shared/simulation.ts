export class Simulation {
    name: string = '';
    email: string = '';
    totalValue: number = 0;
    installmentsValue: number = 1;
    simulationDate: Date = null;
};