import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContractComponent } from './contract/contract.component';
import { SimulatorComponent } from './simulator/simulator.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/simulador',
    pathMatch: 'full' 
  },
  {
    path: 'simulador',
    component: SimulatorComponent
  },
  {
    path: 'contratacao',
    component: ContractComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
