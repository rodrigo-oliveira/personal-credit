import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import { User } from './shared/user';

@Component({
  selector: 'app-contract',
  templateUrl: './contract.component.html',
  styleUrls: ['./contract.component.scss']
})
export class ContractComponent implements OnInit {
  isLinear = true;
  options: string[] = ['One', 'Two', 'Three'];
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  location: Location;

  constructor(private formBuilder: FormBuilder) {}

  ngOnInit() {
    this.createForm(new User());
    this.secondFormGroup = this.formBuilder.group({
      secondCtrl: ['', Validators.required]
    });
    console.log(this.firstFormGroup);
  }

  createForm(user: User) {
    this.firstFormGroup = this.formBuilder.group({
      name: [user.name],
      nationality: [user.nationality],
      state: [user.state]
    });
  }

}
