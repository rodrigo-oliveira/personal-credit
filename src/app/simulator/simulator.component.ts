import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router"
import { SimulatorService } from 'personal-credit-simulator-lib';

@Component({
  selector: 'app-simulator',
  templateUrl: './simulator.component.html',
  styleUrls: ['./simulator.component.scss']
})
export class SimulatorComponent implements OnInit {
  installmentsEndpoint = 'http://localhost:3000/parcelas';

  constructor(private router: Router, private simulatorService: SimulatorService) {
  }

  ngOnInit() {
    this.simulatorService.setEndpoint(this.installmentsEndpoint);
  }

  onSuccessGetInstallments(installments) {
    console.log('Sucesso ao recuperar as parcelas:', installments);
  }

  onErrorGetInstallments(error) {
    console.log('Não foi possível recuperar as parcelas do cliente:', error);
  }

  onSubmitSimulation(simulation) {
    console.log('Informações da simulação:', simulation);
    this.router.navigate(['contratacao']);
  }
}
